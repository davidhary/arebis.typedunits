﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SampleConsoleApplication")]
[assembly: AssemblyDescription("Unit and Amount datatypes library.")]
[assembly: AssemblyCompany("Arebis")]
[assembly: AssemblyProduct("Unit And Amount Library")]
[assembly: AssemblyCopyright("Copyright © Rudi Breedenraedt 2013")]

[assembly: CLSCompliant(true)]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
