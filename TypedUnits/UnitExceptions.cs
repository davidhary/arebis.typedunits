namespace Arebis.TypedUnits
{
    using System;
    using System.Runtime.Serialization;


    /// <summary>
    /// Exception thrown when a unit conversion failed, i.e. because you are converting
    /// amounts from one unit into another non-compatible unit.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1032:Implement standard exception constructors", Justification = "<Pending>" )]
    public class UnitConversionException : InvalidOperationException
    {
        public UnitConversionException() : base() { }

        public UnitConversionException( string message )
            : base( message ) { }

        public UnitConversionException( Unit fromUnit, Unit toUnit )
            : this( $"Failed to convert from unit '{fromUnit.Name}' to unit '{toUnit.Name}'. Units are not compatible and no conversions are defined." ) { }

        protected UnitConversionException( SerializationInfo info, StreamingContext context )
            : base( info, context )
        { }
    }

    /// <summary>
    /// Exception thrown whenever an exception is referenced by name, but no
    /// unit with the given name is known (registered to the UnitManager).
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable", Justification = "<Pending>" )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1032:Implement standard exception constructors", Justification = "<Pending>" )]
    public class UnknownUnitException : Exception
    {

        public UnknownUnitException() : base() { }

        public UnknownUnitException( string message )
            : base( message ) { }

        protected UnknownUnitException( SerializationInfo info, StreamingContext context )
            : base( info, context )
        { }
    }
}
