namespace Arebis.TypedUnits
{
    using System;

    /// <summary> UNit. </summary>
    /// <license> (c) 2013 Rudi Breedenraedt. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    /// SOFTWARE.</para> </license>
    /// <history date="2014-08-27" by="David" revision="1.0.5345"> 
    /// Using http://www.codeproject.com/Articles/611731/Working-with-Units-and-Amounts. </history>
    [Serializable]
    public sealed class Unit : IComparable, IComparable<Unit>, IEquatable<Unit>, IFormattable
    {

        #region Constructor methods

        /// <summary>   Constructor. </summary>
        /// <param name="unit"> The unit. </param>
        public Unit( Unit unit )
            : this( Valid( unit ).Name, Valid( unit ).Symbol, Valid( unit ).Factor, Valid( unit ).UnitType, Valid( unit ).IsNamed )
        {
        }

        /// <summary>   Validates the given unit. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="unit"> The unit. </param>
        /// <returns>   A Unit. </returns>
        private static Unit Valid( Unit unit )
        {
            if ( unit is null )
            {
                throw new ArgumentNullException( nameof( unit ) );
            }

            return unit;
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">     Gets the name of the unit. </param>
        /// <param name="symbol">   Gets the symbol of the unit. </param>
        /// <param name="unitType"> Gets the type of the unit. </param>
        public Unit( string name, string symbol, UnitType unitType )
            : this( name, symbol, 1.0, unitType, true )
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">     Gets the name of the unit. </param>
        /// <param name="symbol">   Gets the symbol of the unit. </param>
        /// <param name="baseUnit"> The base unit. </param>
        public Unit( string name, string symbol, Unit baseUnit )
            : this( name, symbol, baseUnit.Factor, baseUnit.UnitType, true )
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">     Gets the name of the unit. </param>
        /// <param name="symbol">   Gets the symbol of the unit. </param>
        /// <param name="factor">   Gets the factor of the unit. </param>
        /// <param name="unitType"> Gets the type of the unit. </param>
        /// <param name="isNamed">  Whether the unit is named. </param>
        private Unit( string name, string symbol, double factor, UnitType unitType, bool isNamed )
        {
            this.Name = name;
            this.Symbol = symbol;
            this.Factor = factor;
            this.UnitType = unitType;
            this.IsNamed = isNamed;
        }

        /// <summary>
        /// None unit.
        /// </summary>
        public static Unit None { get; } = new Unit( String.Empty, String.Empty, UnitType.None );

        #endregion Constructor methods

        #region Public implementation

        /// <summary>
        /// Gets the name of the unit.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the symbol of the unit.
        /// </summary>
        public string Symbol { get; }

        /// <summary>
        /// Gets the factor of the unit.
        /// </summary>
        public double Factor { get; }

        /// <summary>
        /// Whether the unit is named.
        /// </summary>
        public bool IsNamed { get; }

        /// <summary>
        /// Gets the type of the unit.
        /// </summary>
        public UnitType UnitType { get; }

        /// <summary>
        /// Checks whether the given unit is compatible to this one.
        /// Raises an exception if not compatible.
        /// </summary>
        /// <exception cref="UnitConversionException">Raised when units are not compatible.</exception>
        public void AssertCompatibility( Unit compatibleUnit )
        {
            if ( !this.IsCompatibleTo( compatibleUnit ) )
            {
                throw new UnitConversionException( this, compatibleUnit );
            }
        }

        /// <summary>
        /// Checks whether the passed unit is compatible with this one.
        /// </summary>
        public bool IsCompatibleTo( Unit otherUnit ) => this.UnitType == (otherUnit ?? Unit.None).UnitType;

        /// <summary>
        /// Returns a unit by raising the present unit to the specified power.
        /// I.e. meter.Power(3) would return a cubic meter unit.
        /// </summary>
        public Unit Power( int value ) => new Unit( String.Concat( '(', this.Name, '^', value, ')' ), this.Symbol + '^' + value, ( double ) Math.Pow( ( double ) this.Factor, ( double ) value ), this.UnitType.Power( value ), false );

        /// <summary>
        /// Tests equality of both objects.
        /// </summary>
        public override bool Equals( object obj ) => this.Equals( obj as Unit );


        /// <summary> Tests equality of both objects. </summary>
        /// <param name="other"> The unit to compare to this object. </param>
        /// <returns> <c>true</c> if the objects are considered equal, false if they are not. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0075:Simplify conditional expression", Justification = "<Pending>" )]
        public bool Equals( Unit other )
        {
            return other is null ? false : this.Factor.Equals( other.Factor ) && this.UnitType.Equals( other.UnitType );
        }

        /// <summary>
        /// Returns the hash code of this unit.
        /// </summary>
        public override int GetHashCode() => this.Factor.GetHashCode() ^ this.UnitType.GetHashCode();

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        public override string ToString() => this.ToString( null, null );

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        public string ToString( string format ) => this.ToString( format, null );

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        public string ToString( IFormatProvider formatProvider ) => this.ToString( null, formatProvider );

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        /// <remarks>
        /// The format string can be either 'UN' (Unit Name) or 'US' (Unit Symbol).
        /// </remarks>
        public string ToString( string format, IFormatProvider formatProvider )
        {
            if ( format == null )
            {
                format = "US";
            }

            if ( formatProvider != null )
            {
                // ICustomFormatter formatter = formatProvider.GetFormat(GetType()) as ICustomFormatter;
                if ( formatProvider.GetFormat( this.GetType() ) is ICustomFormatter formatter )
                {
                    return formatter.Format( format, this, formatProvider );
                }
            }

            return format switch
            {
                "UN" => this.Name,
                _ => this.Symbol,
            };
        }

        #endregion Public implementation

        #region Operator overloads

        public static bool operator ==( Unit left, Unit right ) =>
            // return ((object)left == (object)right) || ((object)left != null && (object)right != null && left.Equals(right));
            object.ReferenceEquals( ( object ) left, ( object ) right ) || (!(left is null) && left.Equals( right ));

        public static bool operator !=( Unit left, Unit right ) => (( object ) left != ( object ) right) || left is null || right is null || !left.Equals( right );

        public static Unit Multiply( Unit left, Unit right )
        {
            left ??= Unit.None;
            right ??= Unit.None;
            return new Unit( String.Concat( '(', left.Name, '*', right.Name, ')' ), left.Symbol + '*' + right.Symbol, left.Factor * right.Factor, left.UnitType * right.UnitType, false );
        }

        public static Unit operator *( Unit left, Unit right )
        {
            left ??= Unit.None;
            right ??= Unit.None;
            return new Unit( String.Concat( '(', left.Name, '*', right.Name, ')' ), left.Symbol + '*' + right.Symbol, left.Factor * right.Factor, left.UnitType * right.UnitType, false );
        }

        public static Unit operator *( Unit left, double right ) => right * left;

        public static Unit operator *( double left, Unit right )
        {
            right ??= Unit.None;
            return new Unit( String.Concat( '(', left.ToString(), '*', right.Name, ')' ), left.ToString() + '*' + right.Symbol, left * right.Factor, right.UnitType, false );
        }

        public static Unit operator /( Unit left, Unit right )
        {
            left ??= Unit.None;
            right ??= Unit.None;
            return new Unit( String.Concat( '(', left.Name, '/', right.Name, ')' ), left.Symbol + '/' + right.Symbol, left.Factor / right.Factor, left.UnitType / right.UnitType, false );
        }

        public static Unit operator /( double left, Unit right )
        {
            right ??= Unit.None;
            return new Unit( String.Concat( '(', left.ToString(), '*', right.Name, ')' ), left.ToString() + '*' + right.Symbol, left / right.Factor, right.UnitType.Power( -1 ), false );
        }

        public static Unit operator /( Unit left, double right )
        {
            left ??= Unit.None;
            return new Unit( String.Concat( '(', left.Name, '/', right.ToString(), ')' ), left.Symbol + '/' + right.ToString(), left.Factor / right, left.UnitType, false );
        }

        #endregion Operator overloads

        #region IComparable implementation

        /// <summary>
        /// Compares the passed unit to the current one. Allows sorting units of the same type.
        /// </summary>
        /// <remarks>Only compatible units can be compared.</remarks>
        int IComparable.CompareTo( object obj ) => (( IComparable<Unit> ) this).CompareTo( ( Unit ) obj );

        /// <summary>
        /// Compares the passed unit to the current one. Allows sorting units of the same type.
        /// </summary>
        /// <remarks>Only compatible units can be compared.</remarks>
        int IComparable<Unit>.CompareTo( Unit other )
        {
            if ( other is null )
            {
                throw new ArgumentNullException( nameof( other ) );
            }

            this.AssertCompatibility( other );
            return this.Factor < other.Factor ? -1 : this.Factor > other.Factor ? +1 : 0;
        }

        public static bool operator <( Unit left, Unit right ) => left is null ? right is object : left.CompareTo( right ) < 0;

        private int CompareTo( Unit right ) => this.CompareTo( right );

        public static bool operator <=( Unit left, Unit right ) => left is null || left.CompareTo( right ) <= 0;

        public static bool operator >( Unit left, Unit right ) => left is object && left.CompareTo( right ) > 0;

        public static bool operator >=( Unit left, Unit right ) => left is null ? right is null : left.CompareTo( right ) >= 0;

        #endregion IComparable implementation
    }
}

