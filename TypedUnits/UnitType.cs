namespace Arebis.TypedUnits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Security.Permissions;
    using System.Text;
    using System.Threading;

    [Serializable]
    public sealed class UnitType : ISerializable
    {
        #region BaseUnitType support

        private static readonly ReaderWriterLock BaseUnitTypeLock = new ReaderWriterLock();
        private static readonly IList<string> BaseUnitTypeNames = new List<string>();

        private static string GetBaseUnitName( int index )
        {
            // Lock baseUnitTypeNames:
            BaseUnitTypeLock.AcquireReaderLock( 2000 );

            try
            {
                return BaseUnitTypeNames[index];
            }
            finally
            {
                // Release lock:
                BaseUnitTypeLock.ReleaseReaderLock();
            }
        }

        private static int GetBaseUnitIndex( string unitTypeName )
        {
            // Verify unitTypeName does not contain pipe char (which is used in serializations):
            if ( unitTypeName.Contains( '|' ) )
            {
                throw new ArgumentException( "The name of a UnitType must not contain the '|' (pipe) character.", nameof( unitTypeName ) );
            }

            // Lock baseUnitTypeNames:
            BaseUnitTypeLock.AcquireReaderLock( 2000 );

            try
            {
                // Retrieve index of unitTypeName:
                var index = BaseUnitTypeNames.IndexOf( unitTypeName );

                // If not found, register unitTypeName:
                if ( index == -1 )
                {
                    _ = BaseUnitTypeLock.UpgradeToWriterLock( 2000 );
                    index = BaseUnitTypeNames.Count;
                    BaseUnitTypeNames.Add( unitTypeName );
                }

                // Return index:
                return index;
            }
            finally
            {
                // Release lock:
                _ = BaseUnitTypeLock.ReleaseLock();
            }
        }

        #endregion BaseUnitType support

        private readonly sbyte[] _BaseUnitIndices;

        [NonSerialized]
        private int _CachedHashCode;

        #region Constructor methods

        public UnitType( string unitTypeName )
        {
            var unitIndex = GetBaseUnitIndex( unitTypeName );
            this._BaseUnitIndices = new sbyte[unitIndex + 1];
            this._BaseUnitIndices[unitIndex] = 1;
        }

        private UnitType( int indicesLength ) => this._BaseUnitIndices = new sbyte[indicesLength];

        private UnitType( sbyte[] baseUnitIndices ) => this._BaseUnitIndices = ( sbyte[] ) baseUnitIndices.Clone();

        private UnitType( SerializationInfo info, StreamingContext c )
        {
            // Retrieve data from serialization:
            var tstoreexp = info.GetString( "indexes" ).Split( new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries )
                .Select( x => Convert.ToSByte( x ) )
                .ToArray();
            var tstoreind = info.GetString( "names" ).Split( new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries )
                .Select( x => UnitType.GetBaseUnitIndex( x ) )
                .ToArray();

            // Construct instance:
            if ( tstoreexp.Length > 0 )
            {
                this._BaseUnitIndices = new sbyte[tstoreind.Max() + 1];
                for ( var i = 0; i < tstoreexp.Length; i++ )
                {
                    this._BaseUnitIndices[tstoreind[i]] = tstoreexp[i];
                }
            }
            else
            {
                this._BaseUnitIndices = Array.Empty<sbyte>();
            }
        }

        private static readonly UnitType NoneUnitType = new UnitType( 0 );

        public static UnitType None => UnitType.NoneUnitType;

        #endregion Constructor methods

        #region Public implementation

        /// <summary>
        /// Returns the unit type raised to the specified power.
        /// </summary>
        public UnitType Power( int value )
        {
            var result = new UnitType( this._BaseUnitIndices );
            for ( var i = 0; i < result._BaseUnitIndices.Length; i++ )
            {
                result._BaseUnitIndices[i] = ( sbyte ) (result._BaseUnitIndices[i] * value);
            }

            return result;
        }

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        ///
        /// <param name="obj">  The object to compare with the current object. </param>
        ///
        /// <returns>
        /// true if the specified object  is equal to the current object; otherwise, false.
        /// </returns>
        public override bool Equals( object obj ) => this.Equals( obj as UnitType );

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        ///
        /// <param name="other">    The unit type to compare to this object. </param>
        ///
        /// <returns>
        /// true if the specified object  is equal to the current object; otherwise, false.
        /// </returns>
        public bool Equals( UnitType other )
        {
            if ( other is null )
            {
                return false;
            }
            // Determine longest and shortest baseUnitUndice arrays:
            sbyte[] longest, shortest;
            var leftlen = this._BaseUnitIndices.Length;
            var rightlen = other._BaseUnitIndices.Length;
            if ( leftlen > rightlen )
            {
                longest = this._BaseUnitIndices;
                shortest = other._BaseUnitIndices;
            }
            else
            {
                longest = other._BaseUnitIndices;
                shortest = this._BaseUnitIndices;
            }

            // Compare baseUnitIndice array content:
            for ( var i = 0; i < shortest.Length; i++ )
            {
                if ( shortest[i] != longest[i] )
                {
                    return false;
                }
            }

            for ( var i = shortest.Length; i < longest.Length; i++ )
            {
                if ( longest[i] != 0 )
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            if ( this._CachedHashCode == 0 )
            {
                var hash = 0;
                for ( var i = 0; i < this._BaseUnitIndices.Length; i++ )
                {
                    var factor = i + i + 1;
                    hash += factor * factor * this._BaseUnitIndices[i] * this._BaseUnitIndices[i];
                }
                this._CachedHashCode = hash;
            }
            return this._CachedHashCode;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            var sep = "";
            for ( var i = 0; i < this._BaseUnitIndices.Length; i++ )
            {
                if ( this._BaseUnitIndices[i] != 0 )
                {
                    _ = sb.Append( sep );
                    _ = sb.Append( GetBaseUnitName( i ) );
                    _ = sb.Append( '^' );
                    _ = sb.Append( this._BaseUnitIndices[i] );
                    sep = " * ";
                }
            }
            return sb.ToString();
        }

        #endregion Public implementation

        #region Operator overloads

        public static UnitType operator *( UnitType left, UnitType right )
        {
            var result = new UnitType( Math.Max( left._BaseUnitIndices.Length, right._BaseUnitIndices.Length ) );
            left._BaseUnitIndices.CopyTo( result._BaseUnitIndices, 0 );
            for ( var i = 0; i < right._BaseUnitIndices.Length; i++ )
            {
                result._BaseUnitIndices[i] += right._BaseUnitIndices[i];
            }

            return result;
        }

        public static UnitType operator /( UnitType left, UnitType right )
        {
            var result = new UnitType( Math.Max( left._BaseUnitIndices.Length, right._BaseUnitIndices.Length ) );
            left._BaseUnitIndices.CopyTo( result._BaseUnitIndices, 0 );
            for ( var i = 0; i < right._BaseUnitIndices.Length; i++ )
            {
                result._BaseUnitIndices[i] -= right._BaseUnitIndices[i];
            }

            return result;
        }

        public static bool operator ==( UnitType left, UnitType right ) =>
            // return ((object)left == (object)right) || ((object)left != null && (object)right != null && left.Equals(right));
            object.ReferenceEquals( ( object ) left, ( object ) right ) || (left is object && left.Equals( right ));

        public static bool operator !=( UnitType left, UnitType right ) =>
            // return ((object)left != (object)right) || ((object)left == null || (object)right == null || !left.Equals(right));
            (!object.ReferenceEquals( ( object ) left, ( object ) right )) && (left is null || !left.Equals( right ));

        #endregion Operator overloads

        #region ISerializable Members

        [System.Security.SecurityCritical()]
        void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
        {
            var first = true;
            var sbn = new StringBuilder( this._BaseUnitIndices.Length * 8 );
            var sbx = new StringBuilder( this._BaseUnitIndices.Length * 4 );
            for ( var i = 0; i < this._BaseUnitIndices.Length; i++ )
            {
                if ( this._BaseUnitIndices[i] != 0 )
                {
                    if ( !first )
                    {
                        _ = sbn.Append( '|' );
                    }

                    _ = sbn.Append( UnitType.GetBaseUnitName( i ) );
                    if ( !first )
                    {
                        _ = sbx.Append( '|' );
                    }

                    _ = sbx.Append( this._BaseUnitIndices[i] );
                    first = false;
                }
            }
            if ( info is object )
            {
                info.AddValue( "names", sbn.ToString() );
                info.AddValue( "indexes", sbx.ToString() );
            }
        }


        #endregion
    }
}


