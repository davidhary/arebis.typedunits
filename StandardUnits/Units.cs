namespace Arebis.StandardUnits
{
    using System;

    using Arebis.TypedUnits;

    public static class UnitSymbols
    {
        // http://unicodelookup.com/#greek
        // http://unicodelookup.com/#math
        public static string MU => $"{Convert.ToChar( 0x3BC )}";
        public static string Eta => $"{Convert.ToChar( 0x3BC )}";
        public static string Omega => $"{Convert.ToChar( 0x3A9 )}";
        public static string Sigma => $"{Convert.ToChar( 0x3C3 )}";
        public static string OmegaInverted => $"{Convert.ToChar( 0x2127 )}";
        public static string Squared => $"{Convert.ToChar( 0x2072 )}";
        public static string Cubed => $"{Convert.ToChar( 0x2073 )}";
        public static string Degrees => $"{Convert.ToChar( 0x2070 )}";
        public static string DotProduct => $"{Convert.ToChar( 0x2219 )}";
        public static string WhiteSquare => $"{Convert.ToChar( 0x25A1 )}";
        public static string InvertedOne => $"{Convert.ToChar( 0x196 )}";
    }

    public static class SIUnitTypes
    {
        // DH: Change from meter to Meter; Set all unit name first character to upper case.

        public static UnitType Length => new UnitType( "Meter" );

        public static UnitType Mass => new UnitType( "Kilogram" );

        public static UnitType Time => new UnitType( "Second" );

        public static UnitType ElectricCurrent => new UnitType( "Ampere" );

        public static UnitType ThermodynamicTemperature => new UnitType( "Kelvin" );

        public static UnitType AmountOfSubstance => new UnitType( "Mole" );

        public static UnitType LuminousIntensity => new UnitType( "Candela" );

        public static UnitType Count => new UnitType( "Z-*" );

        public static UnitType Ratio => new UnitType( "Ratio" );

        public static UnitType Bel => new UnitType( "Bel" );

        public static UnitType Neper => new UnitType( "Neper" );

        public static UnitType Percent => new UnitType( "Percent" );

        public static UnitType Hex => new UnitType( "0x" );
    }

    [UnitDefinitionClass]
    public static class LengthUnits
    {
        private static Unit MeterUnit => new Unit( "Meter", "m", SIUnitTypes.Length );

        public static Unit Meter => new Unit( "Meter", "m", MeterUnit );

        public static Unit Micron => new Unit( "Micron", $"{UnitSymbols.MU}m", 0.000001 * Meter );

        public static Unit Millimeter => new Unit( "Millimeter", "mm", 0.001 * Meter );

        public static Unit Centimeter => new Unit( "Centimeter", "cm", 0.01 * Meter );

        public static Unit Decimeter => new Unit( "Decimeter", "dm", 0.1 * Meter );

        public static Unit Decameter => new Unit( "Decameter", "Dm", 10.0 * Meter );

        public static Unit Hectometer => new Unit( "Hectometer", "Hm", 100.0 * Meter );

        public static Unit Kilometer => new Unit( "Kilometer", "km", 1000.0 * Meter );


        public static Unit Inch => new Unit( "Inch", "in", 0.0254 * Meter );

        public static Unit Foot => new Unit( "Foot", "ft", 12.0 * Inch );

        public static Unit Yard => new Unit( "Yard", "yd", 36.0 * Inch );

        public static Unit Mile => new Unit( "Mile", "mi", 5280.0 * Foot );

        public static Unit NauticalMile => new Unit( "Nautical Mile", "nmi", 1852.0 * Meter );


        public static Unit Lightyear => new Unit( "Lightyear", "ly", 9460730472580800.0 * Meter );
    }

    [UnitDefinitionClass]
    public static class SurfaceUnits
    {
        public static Unit SquareMeter => new Unit( "Meter" + UnitSymbols.Squared, "m" + UnitSymbols.Squared, LengthUnits.Meter.Power( 2 ) );

        public static Unit Are => new Unit( "Are", "are", 100.0 * SquareMeter );

        public static Unit Hectare => new Unit( "Hectare", "ha", 10000.0 * SquareMeter );

        public static Unit SquareKilometer => new Unit( "Kilometer" + UnitSymbols.Squared, "Km" + UnitSymbols.Squared, LengthUnits.Kilometer.Power( 2 ) );
    }

    [UnitDefinitionClass]
    public static class VolumeUnits
    {
        public static Unit Liter => new Unit( "Liter", "L", LengthUnits.Decimeter.Power( 3 ) );

        public static Unit Milliliter => new Unit( "Milliliter", "mL", 0.001 * Liter );

        public static Unit Centiliter => new Unit( "Centiliter", "cL", 0.01 * Liter );

        public static Unit Deciliter => new Unit( "Deciliter", "dL", 0.1 * Liter );

        public static Unit CubicMeter => new Unit( "Meter" + UnitSymbols.Cubed, "m" + UnitSymbols.Cubed, LengthUnits.Meter.Power( 3 ) );

        public static Unit CubicFoot => new Unit( "Foot" + UnitSymbols.Cubed, "ft" + UnitSymbols.Cubed, LengthUnits.Foot.Power( 3 ) );

        public static Unit MCF => new Unit( "MCF", "MCF", 1000 * LengthUnits.Foot );

        public static Unit MMCF => new Unit( "MMCF", "MMCF", 1000000 * LengthUnits.Foot );
    }

    [UnitDefinitionClass]
    public static class TimeUnits
    {
        private static Unit SecondUnit => new Unit( "Second", "s", SIUnitTypes.Time );

        public static Unit Second => new Unit( "Second", "s", SecondUnit );

        public static Unit Millisecond => new Unit( "Millisecond", "ms", 0.001 * Second );

        public static Unit Microsecond => new Unit( "Microsecond", UnitSymbols.MU + "s", 0.000001 * Second );

        public static Unit Nanosecond => new Unit( "Nanosecond", UnitSymbols.Eta + "s", 0.000000001 * Second );

        public static Unit Minute => new Unit( "Minute", "min", 60.0 * Second );

        public static Unit Hour => new Unit( "Hour", "h", 3600.0 * Second );

        public static Unit Day => new Unit( "Day", "d", 24.0 * Hour );
    }

    [UnitDefinitionClass]
    public static class SpeedUnits
    {
        public static Unit MeterPerSecond => new Unit( "Meter/Second", "m/s", LengthUnits.Meter / TimeUnits.Second );

        public static Unit KilometerPerHour => new Unit( "Kilometer/Hour", "km/h", LengthUnits.Kilometer / TimeUnits.Hour );

        public static Unit MilePerHour => new Unit( "Mile/Hour", "mi/h", LengthUnits.Mile / TimeUnits.Hour );

        public static Unit Knot => new Unit( "Knot", "kn", 1.852 * SpeedUnits.KilometerPerHour );
    }


    [UnitDefinitionClass]
    public static class FlowUnits
    {
        public static Unit CubicFootPerHour => new Unit( VolumeUnits.CubicFoot.Name + "/" + TimeUnits.Hour.Name,
                                                                VolumeUnits.CubicFoot.Symbol + "/" + TimeUnits.Hour.Symbol,
                                                                VolumeUnits.CubicFoot / TimeUnits.Hour );

        public static Unit MCFPerDay => new Unit( VolumeUnits.MCF.Name + "/" + TimeUnits.Day.Name,
                                                         VolumeUnits.MCF.Symbol + "/" + TimeUnits.Day.Symbol,
                                                         VolumeUnits.MCF / TimeUnits.Day );

        public static Unit MMCFPerDay => new Unit( VolumeUnits.MMCF.Name + "/" + TimeUnits.Day.Name,
                                                     VolumeUnits.MMCF.Symbol + "/" + TimeUnits.Day.Symbol,
                                                     VolumeUnits.MMCF / TimeUnits.Day );
    }

    [UnitDefinitionClass]
    public static class MassUnits
    {
        public static Unit Gram => new Unit( "Gram", "g", 0.001 * Kilogram );

        public static Unit Kilogram => new Unit( "Kilogram", "Kg", SIUnitTypes.Mass );

        public static Unit Milligram => new Unit( "Milligram", "mg", 0.001 * Gram );

        public static Unit Ton => new Unit( "Ton", "ton", 1000.0 * Kilogram );
    }

    [UnitDefinitionClass]
    public static class ForceUnits
    {
        public static Unit Newton => new Unit( "Newton", "N", LengthUnits.Meter * MassUnits.Kilogram * TimeUnits.Second.Power( -2 ) );

        public static Unit Pound => new Unit( "Pound", "lbf", 4.4482216 * ForceUnits.Newton );
    }

    [UnitDefinitionClass]
    public static class ElectricUnits
    {
        public static Unit Ampere => new Unit( "Ampere", "A", SIUnitTypes.ElectricCurrent );

        public static Unit MilliAmpere => new Unit( "Milliampere", $"m{Ampere.Symbol}", 0.001 * Ampere );

        public static Unit Coulomb => new Unit( "Coulomb", "C", TimeUnits.Second * Ampere );

        public static Unit Volt => new Unit( "Volt", "V", EnergyUnits.Watt / Ampere );

        public static Unit Millivolt => new Unit( "Millivolt", $"m{Volt.Symbol}", 0.001 * Volt );

        public static Unit Microvolt => new Unit( "Microvolt", $"{UnitSymbols.MU}{Volt.Symbol}", 0.000001 * Volt );

        public static Unit Ohm => new Unit( "Ohm", UnitSymbols.Omega, Volt / Ampere );

        public static Unit Kilohm => new Unit( "Kilohm", "K" + UnitSymbols.Omega, 1000 * Ohm );

        public static Unit Megohm => new Unit( "Megohm", "M" + UnitSymbols.Omega, 1e+6 * Ohm );

        public static Unit OhmMeter => new Unit( "Ohm-Meter", $"{UnitSymbols.Omega}{UnitSymbols.DotProduct}m", Ohm * LengthUnits.Meter );

        public static Unit OhmPerSquare => new Unit( "Ohm/sq", $"{UnitSymbols.Omega}/{UnitSymbols.WhiteSquare}", Ohm * LengthUnits.Meter );

        public static Unit Mho => new Unit( "Mho", UnitSymbols.OmegaInverted, Ampere / Volt );

        public static Unit Farad => new Unit( "Farad", "F", Coulomb / Volt );

        public static Unit Henry => new Unit( "Henry", "H", Ohm * TimeUnits.Second );

        public static Unit MicroHenry => new Unit( "MicroHenry", $"{UnitSymbols.MU}H", 0.000001 * Henry );

        public static Unit Seebeck => new Unit( "Seebeck", "V/K", Volt / TemperatureUnits.Kelvin );

        public static Unit MicroSeebeck => new Unit( "MicroSeebeck", $"{UnitSymbols.MU}V/K", 0.000001 * Seebeck );
    }

    [UnitDefinitionClass]
    public static class EnergyUnits
    {

        public static Unit Joule => new Unit( "Joule", "J", LengthUnits.Meter.Power( 2 ) * MassUnits.Kilogram * TimeUnits.Second.Power( -2 ) );

        public static Unit Kilojoule => new Unit( "Kilojoule", "KJ", 1000.0 * Joule );

        public static Unit Megajoule => new Unit( "Megajoule", "MJ", 1000000.0 * Joule );

        public static Unit Gigajoule => new Unit( "Gigajoule", "GJ", 1000000000.0 * Joule );

        public static Unit Watt => new Unit( "Watt", "W", Joule / TimeUnits.Second );

        public static Unit Kilowatt => new Unit( "Kilowatt", "kW", 1000.0 * Watt );

        public static Unit Megawatt => new Unit( "Megawatt", "MW", 1000000.0 * Watt );

        public static Unit WattSecond => new Unit( "Watt-Second", "Wsec", Watt * TimeUnits.Second );

        public static Unit WattHour => new Unit( "Watt-Hour", "Wh", Watt * TimeUnits.Hour );

        public static Unit KilowattHour => new Unit( "Kilowatt-Hour", "KWh", 1000.0 * WattHour );

        public static Unit Calorie => new Unit( "Calorie", "cal", 4.1868 * Joule );

        public static Unit Kilocalorie => new Unit( "Kilocalorie", "Kcal", 1000.0 * Calorie );

        public static Unit Horsepower => new Unit( "Horsepower", "hp", 0.73549875 * Kilowatt );
    }

    [UnitDefinitionClass, UnitConversionClass]
    public static class TemperatureUnits
    {
        public static Unit Kelvin => new Unit( "Kelvin", "K", SIUnitTypes.ThermodynamicTemperature );

        public static Unit DegreeCelsius => new Unit( "Degree Celsius", UnitSymbols.Degrees + "C", new UnitType( "Celsius Temperature" ) );

        public static Unit DegreeFahrenheit => new Unit( "Degree Fahrenheit", UnitSymbols.Degrees + "F", new UnitType( "Fahrenheit Temperature" ) );

        public static Unit DegreesCelsiusPerSecond => new Unit( "Deg C/Second", UnitSymbols.Degrees + "C/s", TemperatureUnits.DegreeCelsius / TimeUnits.Second );

        public static Unit DegreesCelsiusPerMinute => new Unit( "Deg C/Minute", UnitSymbols.Degrees + "C/m", TemperatureUnits.DegreeCelsius / TimeUnits.Minute );

        #region Conversion functions

        public static void RegisterConversions()
        {
            // Register conversion functions:

            // Convert Celsius to Fahrenheit:
            UnitManager.RegisterConversion( DegreeCelsius, DegreeFahrenheit, delegate ( Amount amount ) {
                return new Amount( amount.Value * 1.8 + 32.0, DegreeFahrenheit );
            }
            );

            // Convert Fahrenheit to Celsius:
            UnitManager.RegisterConversion( DegreeFahrenheit, DegreeCelsius, delegate ( Amount amount ) {
                return new Amount( (amount.Value - 32.0) / 1.8, DegreeCelsius );
            }
            );

            // Convert Celsius to Kelvin:
            UnitManager.RegisterConversion( DegreeCelsius, Kelvin, delegate ( Amount amount ) {
                return new Amount( amount.Value + 273.15, Kelvin );
            }
            );

            // Convert Kelvin to Celsius:
            UnitManager.RegisterConversion( Kelvin, DegreeCelsius, delegate ( Amount amount ) {
                return new Amount( amount.Value - 273.15, DegreeCelsius );
            }
            );

            // Convert Fahrenheit to Kelvin:
            UnitManager.RegisterConversion( DegreeFahrenheit, Kelvin, delegate ( Amount amount ) {
                return amount.ConvertedTo( DegreeCelsius ).ConvertedTo( Kelvin );
            }
            );

            // Convert Kelvin to Fahrenheit:
            UnitManager.RegisterConversion( Kelvin, DegreeFahrenheit, delegate ( Amount amount ) {
                return amount.ConvertedTo( DegreeCelsius ).ConvertedTo( DegreeFahrenheit );
            }
            );
        }

        #endregion Conversion functions
    }

    [UnitDefinitionClass]
    public static class PressureUnits
    {
        public static Unit Pascal => new Unit( "Pascal", "Pa", ForceUnits.Newton * LengthUnits.Meter.Power( -2 ) );

        public static Unit Hectopascal => new Unit( "Hectopascal", "HPa", 100.0 * Pascal );

        public static Unit Kilopascal => new Unit( "Kilopascal", "KPa", 1000.0 * Pascal );

        public static Unit Bar => new Unit( "Bar", "bar", 100000.0 * Pascal );

        public static Unit Millibar => new Unit( "Millibar", "mbar", 0.001 * Bar );

        public static Unit Atmosphere => new Unit( "Atmosphere", "atm", 101325.0 * Pascal );

        // Pound-force (lbf) per square inch.
        public static Unit PSI => new Unit( "PSI", "psi", 6894.7 * Pascal );

        public static Unit InH2O => new Unit( "Inch H2O", "inH2O", 249.088908333 * Pascal );
    }

    [UnitDefinitionClass]
    public static class FrequencyUnits
    {
        public static Unit Hertz => new Unit( "Hertz", "Hz", TimeUnits.Second.Power( -1 ) );

        public static Unit Kilohertz => new Unit( "Kilohertz", "KHz", 1000.0 * Hertz );

        public static Unit Megahertz => new Unit( "Megahertz", "MHz", 1000000.0 * Hertz );

        public static Unit Gigahertz => new Unit( "Gigahertz", "GHz", 1000000000.0 * Hertz );

        public static Unit RPM => new Unit( "Revolutions per Minute", "rpm", TimeUnits.Minute.Power( -1 ) );
    }

    [UnitDefinitionClass]
    public static class AmountOfSubstanceUnits
    {
        public static Unit Mole => new Unit( "Mole", "mole", SIUnitTypes.AmountOfSubstance );
    }

    [UnitDefinitionClass]
    public static class LuminousIntensityUnits
    {
        public static Unit Candela => new Unit( "Candela", "cd", SIUnitTypes.LuminousIntensity );
    }

    [UnitDefinitionClass]
    public static class UnitlessUnits
    {
        public static Unit Count => new Unit( "Count", UnitSymbols.InvertedOne, SIUnitTypes.Count );

        public static Unit Bel => new Unit( "Bel", "Bel", SIUnitTypes.Bel );

        public static Unit Decibel => new Unit( "Decibel", "dB", 10 * Bel );

        public static Unit Ratio => new Unit( "Ratio", UnitSymbols.InvertedOne, SIUnitTypes.Ratio );

        public static Unit Percent => new Unit( "Percent", "%", 100 * Ratio );

        public static Unit PartsPerMillion => new Unit( "PartsPerMillion", "ppm", 1000000 * Ratio );

        public static Unit Neper => new Unit( "Neper", "Np", SIUnitTypes.Neper );

        public static Unit Status => new Unit( "Status", "Ox", SIUnitTypes.Hex );

        #region Conversion functions
        public static void RegisterConversions()
        {
            // Register conversion functions:

            // Convert Volts to Decibels:
            UnitManager.RegisterConversion( ElectricUnits.Volt, UnitlessUnits.Decibel, delegate ( Amount amount ) {
                return new Amount( 20 * Math.Log10( amount.Value ), UnitlessUnits.Decibel );
            }
            );

            // Convert Watts to Decibels:
            UnitManager.RegisterConversion( EnergyUnits.Watt, UnitlessUnits.Decibel, delegate ( Amount amount ) {
                return new Amount( 10 * Math.Log10( amount.Value ), UnitlessUnits.Decibel );
            }
            );

            // Convert Neper to Decibels:
            UnitManager.RegisterConversion( UnitlessUnits.Neper, UnitlessUnits.Decibel, delegate ( Amount amount ) {
                return new Amount( 0.05 * Math.Log( amount.Value ), UnitlessUnits.Decibel );
            }
            );

            // Convert Decibels to Neper:
            UnitManager.RegisterConversion( UnitlessUnits.Decibel, UnitlessUnits.Neper, delegate ( Amount amount ) {
                return new Amount( 20 * Math.Log10( Math.E ) * amount.Value, UnitlessUnits.Neper );
            }
            );
        }
        #endregion Conversion functions
    }
}
