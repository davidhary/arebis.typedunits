
#pragma warning disable IDE1006 // Naming Styles
namespace Arebis.TypedUnits.AmountTests.My
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Typed Units Amount Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Typed Units Amount Tests";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Arebis.TypedUnits.AmountTests";

    }
}
