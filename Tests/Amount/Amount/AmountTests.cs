using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading;
using System.Xml;

using Arebis.StandardUnits;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Arebis.TypedUnits.AmountTests
{

    /// <summary> Contains unit tests for Amounts. </summary>
    /// <remarks>
    /// (c) 2013 Rudi Breedenraedt. All rights reserved.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2018-01-27, 1.0.5814. Fixed.  </para><para>
    /// David, 2018-04-10, 1.0.6674. Converted to VB.Net. </para>
    /// </remarks>
    [TestClass()]
    public class Amounts
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( isr.Core.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> The camel case. </summary>
        public const int CamelCase = 5;

        /// <summary> The pascal camel case. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private const int _PascalCamelCase = 5;


        /// <summary> The default unit manager. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private UnitManager _DefaultUnitManager;

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{ nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Console.Write( "Resetting the Unit Manager instance..." );
            this._DefaultUnitManager = UnitManager.Instance;
            UnitManager.Instance = new UnitManager();
            UnitManager.RegisterByAssembly( typeof( LengthUnits ).Assembly );
            Console.WriteLine( " done." );
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets or sets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> (Unit Test Method) tests construction 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Construction01Test()
        {
            var a = new Amount( 100d, nameof( VolumeUnits.Liter ) );
            Assert.AreEqual( 100.0d, a.Value );
            Assert.AreEqual( nameof( VolumeUnits.Liter ), a.Unit.Name );
        }

        /// <summary> (Unit Test Method) tests addition. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod]
        public void AdditionTest()
        {
            var a = new Amount( 3000.0d, LengthUnits.Meter );
            var sum = new Amount( 2000.0d, LengthUnits.Meter );
            var expected = new Amount( 5000.0d, LengthUnits.Meter );
            sum += a;
            Console.WriteLine( "Sum = {0}", sum );
            Assert.AreEqual( expected, sum );
        }

        /// <summary> (Unit Test Method) tests addition derived. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod]
        public void AdditionDerivedTest()
        {
            var a = new Amount( 3000.0d, LengthUnits.Meter );
            var sum = new Amount( 2.0d, LengthUnits.Kilometer );
            var expected = new Amount( 5.0d, LengthUnits.Kilometer );
            sum += a;
            Console.WriteLine( "Sum = {0}", sum );
            Assert.AreEqual( expected, sum );
        }

        /// <summary> (Unit Test Method) tests conversion 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Conversion01Test()
        {
            var speed = new Amount( 120d, LengthUnits.Kilometer / TimeUnits.Hour );
            var time = new Amount( 15d, TimeUnits.Minute );
            var distance = (speed * time).ConvertedTo( LengthUnits.Kilometer, 4 );
            Assert.AreEqual( 30.0d, distance.Value );
            Assert.AreEqual( LengthUnits.Kilometer.Name, distance.Unit.Name );
        }

        /// <summary> (Unit Test Method) tests casting 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Casting01Test()
        {
            Amount a = ( Amount ) 350.0d;
            Assert.AreEqual( new Amount( 350.0d, Unit.None ), a );
            var b = new Amount( 123.0d, Unit.None );
            Assert.AreEqual( 123.0d, ( double ) b );
            var c = new Amount( 500.0d, LengthUnits.Meter / LengthUnits.Kilometer );
            Assert.AreEqual( 0.5d, ( double ) c );
            Assert.AreEqual( "15.3", (( Amount ) 15.3d).ToString().Replace( ",", "." ) );
        }

        /// <summary> (Unit Test Method) tests percentage 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Percentage01Test()
        {
            var percent = new Unit( "percent", "%", 0.01d * Unit.None );
            var a = new Amount( 15.0d, percent );
            var b = new Amount( 300.0d, TimeUnits.Minute );
            Assert.AreEqual( "15 %", a.ToString( "0 US" ) );
            Assert.AreEqual( 0.15d, ( double ) a );
            Console.WriteLine( a * b );
            Assert.AreEqual( 45.0d, (a * b).ConvertedTo( TimeUnits.Minute ).Value );
        }

        /// <summary> (Unit Test Method) tests percentage 02. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Percentage02Test()
        {
            var percent = new Unit( "percent", "%", 0.01d * Unit.None );
            var a = new Amount( 2.0d, LengthUnits.Meter );
            var b = new Amount( 17.0d, LengthUnits.Centimeter );
            var p = (b / a).ConvertedTo( percent );
            Assert.AreEqual( "8.50 %", p.ToString( "0.00 US", CultureInfo.InvariantCulture ) );
            Assert.AreEqual( 0.085d, ( double ) p );
        }

        /// <summary> (Unit Test Method) tests power 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Power01Test()
        {
            var a = new Amount( 12.0d, LengthUnits.Meter );
            Assert.AreEqual( new Amount( 1.0d, Unit.None ), a.Power( 0 ) );
            Assert.AreEqual( new Amount( 12.0d, LengthUnits.Meter ), a.Power( 1 ) );
            Assert.AreEqual( new Amount( 144.0d, SurfaceUnits.SquareMeter ), a.Power( 2 ) );
            Assert.AreEqual( new Amount( 1728.0d, VolumeUnits.CubicMeter ), a.Power( 3 ) );
            Assert.AreEqual( new Amount( 1.0d / 12.0d, Unit.None / LengthUnits.Meter ), a.Power( -1 ) );
            Assert.AreEqual( new Amount( 1.0d / 144.0d, Unit.None / SurfaceUnits.SquareMeter ), a.Power( -2 ) );
            Assert.AreEqual( new Amount( 1.0d / 1728.0d, Unit.None / VolumeUnits.CubicMeter ), a.Power( -3 ) );
        }

        /// <summary> (Unit Test Method) tests split 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Split01Test()
        {
            var a = new Amount( 146.0d, TimeUnits.Second );
            var values = a.Split( new Unit[] { TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second }, 0 );
            string separator = string.Empty;
            foreach ( Amount v in values )
            {
                Console.Write( separator );
                Console.Write( v );
                separator = ", ";
            }

            Console.WriteLine();
            Assert.AreEqual( 3, values.Length );
            Assert.AreEqual( new Amount( 0.0d, TimeUnits.Hour ), values[0] );
            Assert.AreEqual( new Amount( 2.0d, TimeUnits.Minute ), values[1] );
            Assert.AreEqual( new Amount( 26.0d, TimeUnits.Second ), values[2] );
        }

        /// <summary> (Unit Test Method) tests split 02. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Split02Test()
        {
            var a = new Amount( 10.5d, LengthUnits.Meter );
            var values = a.Split( new Unit[] { LengthUnits.Yard, LengthUnits.Foot, LengthUnits.Inch }, 1 );
            string separator = string.Empty;
            foreach ( Amount v in values )
            {
                Console.Write( separator );
                Console.Write( v );
                separator = ", ";
            }

            Console.WriteLine();
            Assert.AreEqual( 3, values.Length );
            Assert.AreEqual( new Amount( 11.0d, LengthUnits.Yard ), values[0] );
            Assert.AreEqual( new Amount( 1.0d, LengthUnits.Foot ), values[1] );
            Assert.AreEqual( new Amount( 5.4d, LengthUnits.Inch ), values[2] );
        }

        /// <summary> (Unit Test Method) tests split 03. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Split03Test()
        {
            var a = new Amount( Math.Sqrt( 13d ), LengthUnits.Meter );
            var values = a.Split( new Unit[] { LengthUnits.Meter, LengthUnits.Decimeter, LengthUnits.Centimeter, LengthUnits.Millimeter }, 0 );
            string separator = string.Empty;
            foreach ( Amount v in values )
            {
                Console.Write( separator );
                Console.Write( v );
                separator = ", ";
            }

            Console.WriteLine();
            Assert.AreEqual( new Amount( 3.0d, LengthUnits.Meter ), values[0] );
            Assert.AreEqual( new Amount( 6.0d, LengthUnits.Decimeter ), values[1] );
            Assert.AreEqual( new Amount( 0.0d, LengthUnits.Centimeter ), values[2] );
            Assert.AreEqual( new Amount( 6.0d, LengthUnits.Millimeter ), values[3] );
        }

        /// <summary> (Unit Test Method) tests formatting 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Formatting01Test()
        {
            var defaultCultureInfo = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                var nlbe = CultureInfo.GetCultureInfo( "nl-BE" );
                var enus = CultureInfo.GetCultureInfo( "en-US" );
                var a = new Amount( 12.3456789d, LengthUnits.Kilometer );
                var b = new Amount( 12345.6789d, LengthUnits.Meter );
                var c = new Amount( -0.45d, LengthUnits.Kilometer / TimeUnits.Hour );
                var d = new Amount( 25.678d, LengthUnits.Meter * LengthUnits.Meter );
                Assert.AreEqual( "12.3456789 km", a.ToString() );
                Assert.AreEqual( "12,3456789 Kilometer", a.ToString( "GN", nlbe ) );
                Assert.AreEqual( "12.346 km", a.ToString( "0.000 US", enus ) );
                Assert.AreEqual( "12.346 km", a.ToString( "0.000 US", CultureInfo.CurrentUICulture ) );
                Assert.AreEqual( "12.346", a.ToString( "0.000", CultureInfo.CurrentUICulture ) );
                Assert.AreEqual( "12,35 km", a.ToString( "NS", nlbe ) );
                Assert.AreEqual( "12.35 km", a.ToString( "NS", enus ) );
                Assert.AreEqual( "12.345,68 m", b.ToString( "NS", nlbe ) );
                Assert.AreEqual( "12,345.68 m", b.ToString( "NS", enus ) );
                Assert.AreEqual( "-0.45 km/h", c.ToString( "NS", enus ) );
                Assert.AreEqual( "-0.45 (Kilometer/Hour)", c.ToString( "NN", enus ) );
                Assert.AreEqual( "-0,450 km/h", c.ToString( "0.000 US", nlbe ) );
                Assert.AreEqual( "[0,450] km/h", c.ToString( "0.000 US;[0.000] US", nlbe ) );
                Assert.AreEqual( "12.35 Kilometer", b.ToString( "NN|kilometer" ) );
                Assert.AreEqual( "12.346 km", b.ToString( "#,##0.000 US|kilometer" ) );
                Assert.AreEqual( "+12.346 km", b.ToString( "+#,##0.000 US|kilometer" ) );
                Assert.AreEqual( "12.346 km neg", (-b).ToString( "#,##0.000 US pos;#,##0.000 US neg|kilometer" ) );
                Assert.AreEqual( "25.68 m*m", d.ToString( "NS" ) );
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = defaultCultureInfo;
            }
        }

        /// <summary> (Unit Test Method) tests formatting 08. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Formatting08Test()
        {
            var defaultCultureInfo = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                var a = new Amount( 12.3456789d, LengthUnits.Kilometer );
                Assert.AreEqual( "12.346 km", a.ToString( "{0:G5} {1}", CultureInfo.CurrentUICulture ) );
                Assert.AreEqual( "12.346", a.ToString( "{0:G5}", CultureInfo.CurrentUICulture ) );
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = defaultCultureInfo;
            }
        }

        /// <summary> (Unit Test Method) tests formatting 02. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Formatting02Test()
        {
            var b = new Amount( 1234.5678d, LengthUnits.Meter );
            Assert.AreEqual( "", Amount.ToString( null, "#,##0.000 UN", CultureInfo.InvariantCulture ) );
            Assert.AreEqual( "1,234.568 Meter", Amount.ToString( b, "#,##0.000 UN", CultureInfo.InvariantCulture ) );
        }

        /// <summary> (Unit Test Method) tests formatting 03. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void Formatting03Test()
        {
            var d = new Amount( 278.9d, LengthUnits.Mile );
            var t = new Amount( 2.5d, TimeUnits.Hour );
            var s = d / t;
            Assert.AreEqual( "Taking 2.5 h to travel 449 km means your speed was 179.54 km/h", string.Format( "Taking {1:GG|hour} to travel {0:#,##0 US|kilometer} means your speed was {2:#,##0.00 US|kilometer/hour}", d, t, s ) );
            Amount a = null;
            Assert.AreEqual( "a = ", string.Format( "a = {0:#,##0.0 US}", a ) );
        }

        /// <summary> (Unit Test Method) tests static formatting. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void StaticFormattingTest()
        {
            var a = new Amount( 1234.5678d, LengthUnits.Meter );
            Amount b = null;
            var enus = CultureInfo.GetCultureInfo( "en-US" );
            var nlbe = CultureInfo.GetCultureInfo( "nl-BE" );
            Assert.AreEqual( "1234.5678 m", Amount.ToString( a ).Replace( ",", "." ) );
            Assert.AreEqual( "1234.5678 m", Amount.ToString( a, enus ) );
            Assert.AreEqual( "1234,5678 m", Amount.ToString( a, nlbe ) );
            Assert.AreEqual( "1.234.57 m", Amount.ToString( a, "#,##0.00 US" ).Replace( ",", "." ) );
            Assert.AreEqual( "1,234.57 m", Amount.ToString( a, "#,##0.00 US", enus ) );
            Assert.AreEqual( "1.234,57 m", Amount.ToString( a, "#,##0.00 US", nlbe ) );
            Assert.AreEqual( "", Amount.ToString( b ).Replace( ",", "." ) );
            Assert.AreEqual( "", Amount.ToString( b, enus ) );
            Assert.AreEqual( "", Amount.ToString( b, nlbe ) );
            Assert.AreEqual( "", Amount.ToString( b, "#,##0.00 US" ).Replace( ",", "." ) );
            Assert.AreEqual( "", Amount.ToString( b, "#,##0.00 US", enus ) );
            Assert.AreEqual( "", Amount.ToString( b, "#,##0.00 US", nlbe ) );
            Amount x = null;
            string s = string.Empty;
            Assert.AreEqual( "", s + Amount.ToString( x, "#,##0.00 US|meter" ) );
        }

        /// <summary> (Unit Test Method) tests serialize deserialize 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void SerializeDeserialize01Test()
        {
            // Make some amounts:
            var a1before = new Amount( 12345.6789d, LengthUnits.Meter );
            var a2before = new Amount( -0.45d, LengthUnits.Kilometer / TimeUnits.Hour );
            using var buffer = new MemoryStream();
            // Serialize the units:
            var f = new BinaryFormatter();
            f.Serialize( buffer, a1before );
            f.Serialize( buffer, a2before );

            // Reset stream:
            _ = buffer.Seek( 0L, SeekOrigin.Begin );

            // Deserialize units:
            var g = new BinaryFormatter();
            Amount a1after = ( Amount ) g.Deserialize( buffer );
            Amount a2after = ( Amount ) g.Deserialize( buffer );
            Console.WriteLine( "{0} => {1}", a1before, a1after );
            Console.WriteLine( "{0} => {1}", a2before, a2after );
            Assert.AreEqual( a1before, a1after );
            Assert.AreEqual( a2before, a2after );
        }

        /// <summary> (Unit Test Method) tests null comparison. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void NullComparisonTest()
        {
            Amount a = null;
            Amount b = ( Amount ) 100.0d;
            int result = (( IComparable ) b).CompareTo( a );
            Assert.IsTrue( result > 0 );
        }

        /// <summary> (Unit Test Method) tests addition with null. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AdditionWithNullTest()
        {
            Amount a, b, sum;

            // Test both not null:
            a = new Amount( 100.0d, LengthUnits.Meter );
            b = new Amount( 25.0d, LengthUnits.Meter );
            sum = a + b;
            Assert.AreEqual( new Amount( 125.0d, LengthUnits.Meter ), sum );

            // Test right not null:
            a = new Amount( 100.0d, LengthUnits.Meter );
            b = null;
            sum = a + b;
            // not consistent with .NET treatment of Nullable: 
            // Assert.AreEqual(new Amount(100.0, LengthUnits.Meter), sum);
            Assert.IsNull( sum );

            // Test left not null:
            a = null;
            b = new Amount( 25.0d, LengthUnits.Meter );
            sum = a + b;
            // not consistent with .NET treatment of Nullable: 
            // Assert.AreEqual(new Amount(25.0, LengthUnits.Meter), sum);
            Assert.IsNull( sum );

            // Test both null:
            a = null;
            b = null;
            sum = a + b;
            Assert.AreEqual( null, sum );
        }

        /// <summary> (Unit Test Method) tests subtract with null. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void SubtractWithNullTest()
        {
            Amount a, b, subs;

            // Test both not null:
            a = new Amount( 100.0d, LengthUnits.Meter );
            b = new Amount( 25.0d, LengthUnits.Meter );
            subs = a - b;
            Assert.AreEqual( new Amount( 75.0d, LengthUnits.Meter ), subs );

            // Test right not null:
            a = new Amount( 100.0d, LengthUnits.Meter );
            b = null;
            subs = a - b;
            // not consistent with .NET treatment of Nullable: 
            // Assert.AreEqual(new Amount(100.0, LengthUnits.Meter), subs);
            Assert.IsNull( subs );

            // Test left not null:
            a = null;
            b = new Amount( 25.0d, LengthUnits.Meter );
            subs = a - b;
            // not consistent with .NET treatment of Nullable: 
            // Assert.AreEqual(new Amount(-25.0, LengthUnits.Meter), subs);
            Assert.IsNull( subs );

            // Test both null:
            a = null;
            b = null;
            subs = a - b;
            Assert.AreEqual( null, subs );
        }

        /// <summary> (Unit Test Method) tests rounded comparison. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void RoundedComparisonTest()
        {
            var a = new Amount( 0.045d, LengthUnits.Meter );
            var b = new Amount( 0.0450000000001d, LengthUnits.Meter );
            var c = new Amount( 0.0450000000002d, LengthUnits.Meter );
            var d = new Amount( 0.046d, LengthUnits.Meter );
            Assert.IsFalse( a.Value == b.Value );
            Assert.IsFalse( b.Value == c.Value );
            Assert.IsFalse( a.Value == c.Value );
            Assert.IsTrue( a == b );
            Assert.IsTrue( b == c );
            Assert.IsTrue( a == c );
            Assert.IsFalse( c == d );
            Assert.IsTrue( a.Equals( b ) );
            Assert.IsTrue( b.Equals( c ) );
            Assert.IsTrue( a.Equals( c ) );
            Assert.IsFalse( c.Equals( d ) );
        }

        /// <summary> (Unit Test Method) tests comparison 01. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod]
        public void Comparison01Test()
        {
            double value = -0.00002d;
            var a = new Amount( value, EnergyUnits.Horsepower );
            var b = new Amount( value, EnergyUnits.Horsepower );
            var ar = a.ConvertedTo( EnergyUnits.Watt );
            var br = b.ConvertedTo( EnergyUnits.Watt );
            Assert.IsTrue( a == b, $"horsepower: {a} equals {b}" );
            Assert.IsFalse( a > b, $"horsepower: {a} is not greater {b}" );
            Assert.IsFalse( a < b, $"horsepower: {a} is not smaller than {b}" );
            Assert.IsTrue( ar == br, $"watt: {ar} equals {br}" );
            Assert.IsFalse( ar > br, $"watt: a {ar} is not larger than b {br}" );
            Assert.IsFalse( ar < br, $"watt: a {ar} is not smaller than b {br}" );
        }

        /// <summary> (Unit Test Method) tests comparison 02. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod]
        public void Comparison02Test()
        {
            var a = new Amount( 120.0d, SpeedUnits.KilometerPerHour );
            var b = new Amount( 33.333333333d, SpeedUnits.MeterPerSecond );
            Assert.IsTrue( a == b );
            Assert.IsFalse( a < b );
            Assert.IsFalse( a > b );
            Assert.IsTrue( a <= b );
            Assert.IsTrue( a >= b );
            Assert.IsFalse( a != b );
        }

        /// <summary> (Unit Test Method) tests division by zero. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod]
        public void DivisionByZeroTest()
        {
            var d1 = new Amount( 32.0d, LengthUnits.Kilometer );
            var d2 = new Amount( 0.0d, LengthUnits.Kilometer );
            var t = new Amount( 0.0d, TimeUnits.Hour );
            Amount s;
            s = d1 / t;
            Assert.IsTrue( double.IsInfinity( s.Value ) );
            Assert.IsTrue( double.IsPositiveInfinity( s.Value ) );
            Assert.AreEqual( s.Unit, d1.Unit / t.Unit );
            s = d2 / t;
            Assert.IsTrue( double.IsNaN( s.Value ) );
            Assert.AreEqual( s.Unit, d2.Unit / t.Unit );
        }

        /// <summary>
        /// (Unit Test Method) tests amount net data contract serializer serialization.
        /// </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountNetDataContractSerializerSerializationTest()
        {
            var a = new Amount( 3500.12d, EnergyUnits.KilowattHour * (365.0d * TimeUnits.Day) / VolumeUnits.CubicMeter );

            // Serialize instance:
            using var stream = new MemoryStream();
            var serializer = new NetDataContractSerializer();
            serializer.Serialize( stream, a );

            // Deserialize instance:
            stream.Position = 0L;
            Amount b = ( Amount ) serializer.Deserialize( stream );

            // Show serialization (this closes the stream):
            stream.Position = 0L;
            Console.WriteLine( stream.ToXmlString() );
            Console.WriteLine();

            // Compare:
            Console.WriteLine( a );
            Console.WriteLine( b );
            Assert.AreEqual( a, b );
        }

        /// <summary>
        /// (Unit Test Method) tests amount array net data contract serializer serialization.
        /// </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountArrayNetDataContractSerializerSerializationTest()
        {
            var aa = new Amount[6];
            aa[0] = new Amount( 32.5d, LengthUnits.NauticalMile );
            aa[1] = new Amount( 3500.12d, EnergyUnits.KilowattHour * (365.0d * TimeUnits.Day) / VolumeUnits.CubicMeter );
            aa[2] = 3d * aa[0];
            aa[3] = 3d * aa[1];
            aa[4] = aa[1] / aa[3];
            aa[5] = new Amount( 42.3d, LengthUnits.Meter / TimeUnits.Second.Power( 2 ) );

            // Serialize instance:
            using var stream = new MemoryStream();
            var serializer = new NetDataContractSerializer();
            serializer.WriteObject( stream, aa );

            // Deserialize instance:
            stream.Position = 0L;
            Amount[] ba = ( Amount[] ) serializer.ReadObject( stream );

            // Show serialization (this closes the stream):
            stream.Position = 0L;
            Console.WriteLine( stream.ToXmlString() );
            Console.WriteLine();

            // Compare:
            Assert.AreEqual( aa.Length, ba.Length );
            for ( int i = 0, loopTo = aa.Length - 1; i <= loopTo; i++ )
            {
                Console.WriteLine( aa[i] );
                Console.WriteLine( ba[i] );
                Assert.AreEqual( aa[i], ba[i] );
            }
        }

        /// <summary>
        /// (Unit Test Method) tests amount data contract serializer serialization.
        /// </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountDataContractSerializerSerializationTest()
        {
            var a = new Amount( 3500.12d, EnergyUnits.KilowattHour * (365.0d * TimeUnits.Day) / VolumeUnits.CubicMeter );

            // Serialize instance:
            using var stream = new MemoryStream();
            var serializer = new DataContractSerializer( typeof( Amount ) );
            serializer.WriteObject( stream, a );

            // Deserialize instance:
            stream.Position = 0L;
            Amount b = ( Amount ) serializer.ReadObject( stream );

            // Show serialization (this closes the stream):
            stream.Position = 0L;
            Console.WriteLine( stream.ToXmlString() );
            Console.WriteLine();


            // Compare:
            Console.WriteLine( a );
            Console.WriteLine( b );
            Assert.AreEqual( a, b );
        }

        /// <summary>
        /// (Unit Test Method) tests amount array data contract serializer serialization.
        /// </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountArrayDataContractSerializerSerializationTest()
        {
            var aa = new Amount[6];
            aa[0] = new Amount( 32.5d, LengthUnits.NauticalMile );
            aa[1] = new Amount( 3500.12d, EnergyUnits.KilowattHour * (365.0d * TimeUnits.Day) / VolumeUnits.CubicMeter );
            aa[2] = 3d * aa[0];
            aa[3] = 3d * aa[1];
            aa[4] = aa[1] / aa[3];
            aa[5] = new Amount( 42.3d, LengthUnits.Meter / TimeUnits.Second.Power( 2 ) );

            // Serialize instance:
            using var stream = new MemoryStream();
            var serializer = new DataContractSerializer( typeof( Amount[] ) );
            serializer.WriteObject( stream, aa );

            // Deserialize instance:
            stream.Position = 0L;
            Amount[] ba = ( Amount[] ) serializer.ReadObject( stream );

            // Show serialization (this closes the stream):
            stream.Position = 0L;
            Console.WriteLine( stream.ToXmlString() );
            Console.WriteLine();

            // Compare:
            Assert.AreEqual( aa.Length, ba.Length );
            for ( int i = 0, loopTo = aa.Length - 1; i <= loopTo; i++ )
            {
                Console.WriteLine( aa[i] );
                Console.WriteLine( ba[i] );
                Assert.AreEqual( aa[i], ba[i] );
            }
        }

        /// <summary> (Unit Test Method) tests amount binary formatter serialization. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountBinaryFormatterSerializationTest()
        {
            var a = new Amount( 3500.12d, EnergyUnits.KilowattHour * (365.0d * TimeUnits.Day) / VolumeUnits.CubicMeter );

            // Serialize instance:
            using var stream = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize( stream, a );

            // Deserialize instance:
            stream.Position = 0L;
            Amount b = ( Amount ) formatter.Deserialize( stream );

            // Compare:
            Console.WriteLine( a );
            Console.WriteLine( b );
            Assert.AreEqual( a, b );
        }

        /// <summary> (Unit Test Method) tests amount SOAP formatter serialization. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountSoapFormatterSerializationTest()
        {
            var a = new Amount( 3500.12d, EnergyUnits.KilowattHour * (365.0d * TimeUnits.Day) / VolumeUnits.CubicMeter );

            // Serialize instance:
            using var stream = new MemoryStream();
            var formatter = new SoapFormatter();
            formatter.Serialize( stream, a );

            // Deserialize instance:
            stream.Position = 0L;
            Amount b = ( Amount ) formatter.Deserialize( stream );

            // Show serialization (this closes the stream):
            stream.Position = 0L;
            Console.WriteLine( stream.ToXmlString() );
            Console.WriteLine();

            // Compare:
            Console.WriteLine( a );
            Console.WriteLine( b );
            Assert.AreEqual( a, b );
        }

        /// <summary> (Unit Test Method) tests amount compatibility. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountCompatibilityTest()
        {
            var a = new Amount( 300d, LengthUnits.Mile / TimeUnits.Hour.Power( 2 ) );
            Assert.IsTrue( a.Unit.IsCompatibleTo( LengthUnits.Meter / TimeUnits.Second.Power( 2 ) ) );
            Assert.IsTrue( a.Unit.IsCompatibleTo( LengthUnits.Meter * TimeUnits.Second.Power( -2 ) ) );
            Assert.IsFalse( a.Unit.IsCompatibleTo( LengthUnits.Meter / TimeUnits.Second.Power( 1 ) ) );
            Assert.IsFalse( a.Unit.IsCompatibleTo( MassUnits.Gram ) );
            Console.WriteLine( a.Unit.UnitType.ToString() );
        }

        /// <summary> (Unit Test Method) tests amount split. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountSplitTest()
        {
            // One fifth of a week:
            var a = new Amount( 1.0d / 5.0d, TimeUnits.Day * 7.0d );
            var result = a.Split( new Unit[] { TimeUnits.Day, TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second }, 3 );
            foreach ( Amount item in result )
                Console.WriteLine( item );
            Assert.AreEqual( 4, result.Length );
            CollectionAssert.AreEqual( new double[] { 1.0d, 9.0d, 36.0d, 0.0d }.ToList(), result.Select( x => x.Value ).ToList() );
        }

        /// <summary> (Unit Test Method) tests amount split 2. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        [TestMethod()]
        public void AmountSplit2Test()
        {
            // One fifth of a week:
            var a = new Amount( 7.0d / 5.0d, TimeUnits.Day );
            var result = a.Split( new Unit[] { TimeUnits.Day, TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second }, 3 );
            foreach ( Amount item in result )
                Console.WriteLine( item );

            // In this case, the split results in 1 day, 9 hours, 35 minutes and 60 SECONDS!
            // This is due to rounding; it results in ..., 35 minutes and 59.99999 seconds,
            // which once rounded, end up to be 60 seconds...

            Assert.AreEqual( 4, result.Length );
            CollectionAssert.AreEqual( new double[] { 1.0d, 9.0d, 35.0d, 60.0d }.ToList(), result.Select( x => x.Value ).ToList() );
        }

        /// <summary> (Unit Test Method) tests amount split incompatible. </summary>
        /// <remarks> David, 2020-03-07. </remarks>
        [TestMethod()]
        public void AmountSplitIncompatibleTest()
        {
            // One fifth of a week:
            var a = new Amount( 7.0d / 5.0d, TimeUnits.Day );
            var amounts = a.Split( new Unit[] { TimeUnits.Day, TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second }, 3 );
            var d = Amount.Combine( amounts, TimeUnits.Day );
            Assert.IsTrue( a.Equals( d ), $"Combined amounts {d} should equals {a}" );
        }
    }

    internal static class StreamExtensions
    {

        /// <summary> Converts a stream to an XML string. </summary>
        /// <remarks> David, 2020-10-05. </remarks>
        /// <param name="stream"> The stream. </param>
        /// <returns> Stream as a String. </returns>
        public static string ToXmlString( this Stream stream )
        {
            var settings = new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true };
            using var reader = new StreamReader( stream );
            var nt = new NameTable();
            var doc = new XmlDocument();
            doc.LoadXml( reader.ReadToEnd() );
            var sb = new StringBuilder();
            using ( var xwri = XmlWriter.Create( sb, settings ) )
            {
                doc.WriteTo( xwri );
            }

            return sb.ToString();
        }
    }
}
