using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Arebis Typed Units Tests")]
[assembly: AssemblyDescription( "Arebis Typed Units Tests" )]
[assembly: AssemblyProduct( "Arebis.Typed.Units.Tests" )]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
