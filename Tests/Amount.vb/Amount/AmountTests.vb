Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization.Formatters.Soap
Imports System.Text
Imports System.Xml
Imports Arebis.StandardUnits
Imports System.Globalization
Imports System.Threading

''' <summary> Contains unit tests for Amounts. </summary>
''' <remarks>
''' (c) 2013 Rudi Breedenraedt. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/27/2018, 1.0.5814 Fixed.  </para><para>
''' David, 4/10/2018, 1.0.6674 Converted to VB.Net.</para>
''' </remarks>
<TestClass()>
Public Class Amounts

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestInfo = New TestSite
            TestInfo.AddTraceMessagesQueue(_TestInfo.TraceMessagesQueueListener)
            TestInfo.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        If TestInfo IsNot Nothing Then _TestInfo.Dispose() : _TestInfo = Nothing
    End Sub

    ''' <summary> The camel case. </summary>
    Public Const CamelCase As Integer = 5

    ''' <summary> The pascal camel case. </summary>
    Private Const _PascalCamelCase As Integer = 5

    ''' <summary> The default unit manager. </summary>
    Private _DefaultUnitManager As UnitManager

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Console.Write("Resetting the Unit Manager instance...")
        Me._DefaultUnitManager = UnitManager.Instance
        UnitManager.Instance = New UnitManager()
        UnitManager.RegisterByAssembly(GetType(LengthUnits).Assembly)
        Console.WriteLine(" done.")
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> Gets or sets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite

#End Region

    ''' <summary> (Unit Test Method) tests construction 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Construction01Test()
        Dim a As New Amount(100, NameOf(Arebis.StandardUnits.VolumeUnits.Liter))
        Assert.AreEqual(100.0, a.Value)
        Assert.AreEqual(NameOf(Arebis.StandardUnits.VolumeUnits.Liter), a.Unit.Name)
    End Sub

    ''' <summary> (Unit Test Method) tests addition. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod>
    Public Sub AdditionTest()
        Dim a As New Amount(3000.0, LengthUnits.Meter)
        Dim sum As New Amount(2000.0, LengthUnits.Meter)
        Dim expected As New Amount(5000.0, LengthUnits.Meter)

        sum += a

        Console.WriteLine("Sum = {0}", sum)
        Assert.AreEqual(expected, sum)
    End Sub

    ''' <summary> (Unit Test Method) tests addition derived. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod>
    Public Sub AdditionDerivedTest()
        Dim a As New Amount(3000.0, LengthUnits.Meter)
        Dim sum As New Amount(2.0, LengthUnits.Kilometer)
        Dim expected As New Amount(5.0, LengthUnits.Kilometer)

        sum += a

        Console.WriteLine("Sum = {0}", sum)
        Assert.AreEqual(expected, sum)
    End Sub

    ''' <summary> (Unit Test Method) tests conversion 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Conversion01Test()
        Dim speed As New Amount(120, LengthUnits.Kilometer / TimeUnits.Hour)
        Dim time As New Amount(15, TimeUnits.Minute)
        Dim distance As Amount = (speed * time).ConvertedTo(LengthUnits.Kilometer, 4)
        Assert.AreEqual(30.0, distance.Value)
        Assert.AreEqual(LengthUnits.Kilometer.Name, distance.Unit.Name)
    End Sub

    ''' <summary> (Unit Test Method) tests casting 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Casting01Test()
        Dim a As Amount = CType(350.0, Amount)
        Assert.AreEqual(New Amount(350.0, Unit.None), a)

        Dim b As New Amount(123.0, Unit.None)
        Assert.AreEqual(123.0, CDbl(b))

        Dim c As New Amount(500.0, LengthUnits.Meter / LengthUnits.Kilometer)
        Assert.AreEqual(0.5, CDbl(c))

        Assert.AreEqual("15.3", CType(15.3, Amount).ToString().Replace(",", "."))
    End Sub

    ''' <summary> (Unit Test Method) tests percentage 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Percentage01Test()
        Dim percent As New Unit("percent", "%", 0.01 * Unit.None)

        Dim a As New Amount(15.0, percent)
        Dim b As New Amount(300.0, TimeUnits.Minute)

        Assert.AreEqual("15 %", a.ToString("0 US"))
        Assert.AreEqual(0.15, CDbl(a))
        Console.WriteLine(a * b)
        Assert.AreEqual(45.0, (a * b).ConvertedTo(TimeUnits.Minute).Value)
    End Sub

    ''' <summary> (Unit Test Method) tests percentage 02. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Percentage02Test()
        Dim percent As New Unit("percent", "%", 0.01 * Unit.None)

        Dim a As New Amount(2.0, LengthUnits.Meter)
        Dim b As New Amount(17.0, LengthUnits.Centimeter)

        Dim p As Amount = (b / a).ConvertedTo(percent)

        Assert.AreEqual("8.50 %", p.ToString("0.00 US", CultureInfo.InvariantCulture))
        Assert.AreEqual(0.085, CDbl(p))
    End Sub

    ''' <summary> (Unit Test Method) tests power 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Power01Test()
        Dim a As New Amount(12.0, LengthUnits.Meter)

        Assert.AreEqual(New Amount(1.0, Unit.None), a.Power(0))

        Assert.AreEqual(New Amount(12.0, LengthUnits.Meter), a.Power(1))
        Assert.AreEqual(New Amount(144.0, SurfaceUnits.SquareMeter), a.Power(2))
        Assert.AreEqual(New Amount(1728.0, VolumeUnits.CubicMeter), a.Power(3))

        Assert.AreEqual(New Amount(1.0 / 12.0, Unit.None / LengthUnits.Meter), a.Power(-1))
        Assert.AreEqual(New Amount(1.0 / 144.0, Unit.None / SurfaceUnits.SquareMeter), a.Power(-2))
        Assert.AreEqual(New Amount(1.0 / 1728.0, Unit.None / VolumeUnits.CubicMeter), a.Power(-3))
    End Sub

    ''' <summary> (Unit Test Method) tests split 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Split01Test()
        Dim a As New Amount(146.0, TimeUnits.Second)
        Dim values() As Amount = a.Split(New Unit() {TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second}, 0)

        Dim separator As String = String.Empty
        For Each v As Amount In values
            Console.Write(separator)
            Console.Write(v)
            separator = ", "
        Next v
        Console.WriteLine()

        Assert.AreEqual(3, values.Length)
        Assert.AreEqual(New Amount(0.0, TimeUnits.Hour), values(0))
        Assert.AreEqual(New Amount(2.0, TimeUnits.Minute), values(1))
        Assert.AreEqual(New Amount(26.0, TimeUnits.Second), values(2))
    End Sub

    ''' <summary> (Unit Test Method) tests split 02. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Split02Test()
        Dim a As New Amount(10.5, LengthUnits.Meter)
        Dim values() As Amount = a.Split(New Unit() {LengthUnits.Yard, LengthUnits.Foot, LengthUnits.Inch}, 1)

        Dim separator As String = String.Empty
        For Each v As Amount In values
            Console.Write(separator)
            Console.Write(v)
            separator = ", "
        Next v
        Console.WriteLine()

        Assert.AreEqual(3, values.Length)
        Assert.AreEqual(New Amount(11.0, LengthUnits.Yard), values(0))
        Assert.AreEqual(New Amount(1.0, LengthUnits.Foot), values(1))
        Assert.AreEqual(New Amount(5.4, LengthUnits.Inch), values(2))
    End Sub

    ''' <summary> (Unit Test Method) tests split 03. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Split03Test()
        Dim a As New Amount(Global.System.Math.Sqrt(13), LengthUnits.Meter)
        Dim values() As Amount = a.Split(New Unit() {LengthUnits.Meter, LengthUnits.Decimeter, LengthUnits.Centimeter, LengthUnits.Millimeter}, 0)

        Dim separator As String = String.Empty
        For Each v As Amount In values
            Console.Write(separator)
            Console.Write(v)
            separator = ", "
        Next v
        Console.WriteLine()

        Assert.AreEqual(New Amount(3.0, LengthUnits.Meter), values(0))
        Assert.AreEqual(New Amount(6.0, LengthUnits.Decimeter), values(1))
        Assert.AreEqual(New Amount(0.0, LengthUnits.Centimeter), values(2))
        Assert.AreEqual(New Amount(6.0, LengthUnits.Millimeter), values(3))
    End Sub

    ''' <summary> (Unit Test Method) tests formatting 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Formatting01Test()
        Dim defaultCultureInfo As CultureInfo = Thread.CurrentThread.CurrentCulture
        Try
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture

            Dim nlbe As CultureInfo = CultureInfo.GetCultureInfo("nl-BE")
            Dim enus As CultureInfo = CultureInfo.GetCultureInfo("en-US")

            Dim a As New Amount(12.3456789, LengthUnits.Kilometer)
            Dim b As New Amount(12345.6789, LengthUnits.Meter)
            Dim c As New Amount(-0.45, LengthUnits.Kilometer / TimeUnits.Hour)
            Dim d As New Amount(25.678, LengthUnits.Meter * LengthUnits.Meter)

            Assert.AreEqual("12.3456789 km", a.ToString())
            Assert.AreEqual("12,3456789 Kilometer", a.ToString("GN", nlbe))
            Assert.AreEqual("12.346 km", a.ToString("0.000 US", enus))
            Assert.AreEqual("12.346 km", a.ToString("0.000 US", CultureInfo.CurrentUICulture))
            Assert.AreEqual("12.346", a.ToString("0.000", CultureInfo.CurrentUICulture))
            Assert.AreEqual("12,35 km", a.ToString("NS", nlbe))
            Assert.AreEqual("12.35 km", a.ToString("NS", enus))
            Assert.AreEqual("12.345,68 m", b.ToString("NS", nlbe))
            Assert.AreEqual("12,345.68 m", b.ToString("NS", enus))
            Assert.AreEqual("-0.45 km/h", c.ToString("NS", enus))
            Assert.AreEqual("-0.45 (Kilometer/Hour)", c.ToString("NN", enus))
            Assert.AreEqual("-0,450 km/h", c.ToString("0.000 US", nlbe))
            Assert.AreEqual("[0,450] km/h", c.ToString("0.000 US;[0.000] US", nlbe))
            Assert.AreEqual("12.35 Kilometer", b.ToString("NN|kilometer"))
            Assert.AreEqual("12.346 km", b.ToString("#,##0.000 US|kilometer"))
            Assert.AreEqual("+12.346 km", b.ToString("+#,##0.000 US|kilometer"))
            Assert.AreEqual("12.346 km neg", (-b).ToString("#,##0.000 US pos;#,##0.000 US neg|kilometer"))
            Assert.AreEqual("25.68 m*m", d.ToString("NS"))
        Finally
            Thread.CurrentThread.CurrentCulture = defaultCultureInfo
        End Try
    End Sub

    ''' <summary> (Unit Test Method) tests formatting 08. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Formatting08Test()
        Dim defaultCultureInfo As CultureInfo = Thread.CurrentThread.CurrentCulture
        Try
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture
            Dim a As New Amount(12.3456789, LengthUnits.Kilometer)
            Assert.AreEqual("12.346 km", a.ToString("{0:G5} {1}", CultureInfo.CurrentUICulture))
            Assert.AreEqual("12.346", a.ToString("{0:G5}", CultureInfo.CurrentUICulture))
        Finally
            Thread.CurrentThread.CurrentCulture = defaultCultureInfo
        End Try
    End Sub

    ''' <summary> (Unit Test Method) tests formatting 02. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Formatting02Test()
        Dim b As New Amount(1234.5678, LengthUnits.Meter)
        Assert.AreEqual("", Amount.ToString(Nothing, "#,##0.000 UN", CultureInfo.InvariantCulture))
        Assert.AreEqual("1,234.568 Meter", Amount.ToString(b, "#,##0.000 UN", CultureInfo.InvariantCulture))
    End Sub

    ''' <summary> (Unit Test Method) tests formatting 03. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub Formatting03Test()
        Dim d As New Amount(278.9, LengthUnits.Mile)
        Dim t As New Amount(2.5, TimeUnits.Hour)

        Dim s As Amount = d / t

        Assert.AreEqual("Taking 2.5 h to travel 449 km means your speed was 179.54 km/h", String.Format("Taking {1:GG|hour} to travel {0:#,##0 US|kilometer} means your speed was {2:#,##0.00 US|kilometer/hour}", d, t, s))

        Dim a As Amount = Nothing

        Assert.AreEqual("a = ", String.Format("a = {0:#,##0.0 US}", a))
    End Sub

    ''' <summary> (Unit Test Method) tests static formatting. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub StaticFormattingTest()
        Dim a As New Amount(1234.5678, LengthUnits.Meter)
        Dim b As Amount = Nothing

        Dim enus As CultureInfo = CultureInfo.GetCultureInfo("en-US")
        Dim nlbe As CultureInfo = CultureInfo.GetCultureInfo("nl-BE")

        Assert.AreEqual("1234.5678 m", Amount.ToString(a).Replace(",", "."))
        Assert.AreEqual("1234.5678 m", Amount.ToString(a, enus))
        Assert.AreEqual("1234,5678 m", Amount.ToString(a, nlbe))
        Assert.AreEqual("1.234.57 m", Amount.ToString(a, "#,##0.00 US").Replace(",", "."))
        Assert.AreEqual("1,234.57 m", Amount.ToString(a, "#,##0.00 US", enus))
        Assert.AreEqual("1.234,57 m", Amount.ToString(a, "#,##0.00 US", nlbe))

        Assert.AreEqual("", Amount.ToString(b).Replace(",", "."))
        Assert.AreEqual("", Amount.ToString(b, enus))
        Assert.AreEqual("", Amount.ToString(b, nlbe))
        Assert.AreEqual("", Amount.ToString(b, "#,##0.00 US").Replace(",", "."))
        Assert.AreEqual("", Amount.ToString(b, "#,##0.00 US", enus))
        Assert.AreEqual("", Amount.ToString(b, "#,##0.00 US", nlbe))

        Dim x As Amount = Nothing
        Dim s As String = String.Empty
        Assert.AreEqual("", s & Amount.ToString(x, "#,##0.00 US|meter"))

    End Sub

    ''' <summary> (Unit Test Method) tests serialize deserialize 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub SerializeDeserialize01Test()
        ' Make some amounts:
        Dim a1before As New Amount(12345.6789, LengthUnits.Meter)
        Dim a2before As New Amount(-0.45, LengthUnits.Kilometer / TimeUnits.Hour)

        Using buffer As New MemoryStream()
            ' Serialize the units:
            Dim f As New BinaryFormatter()
            f.Serialize(buffer, a1before)
            f.Serialize(buffer, a2before)

            ' Reset stream:
            buffer.Seek(0, SeekOrigin.Begin)

            ' Deserialize units:
            Dim g As New BinaryFormatter()
            Dim a1after As Amount = CType(g.Deserialize(buffer), Amount)
            Dim a2after As Amount = CType(g.Deserialize(buffer), Amount)

            Console.WriteLine("{0} => {1}", a1before, a1after)
            Console.WriteLine("{0} => {1}", a2before, a2after)

            Assert.AreEqual(a1before, a1after)
            Assert.AreEqual(a2before, a2after)
        End Using

    End Sub

    ''' <summary> (Unit Test Method) tests null comparison. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub NullComparisonTest()
        Dim a As Amount = Nothing
        Dim b As Amount = CType(100.0, Amount)

        Dim result As Integer = CType(b, IComparable).CompareTo(a)

        Assert.IsTrue(result > 0)
    End Sub

    ''' <summary> (Unit Test Method) tests addition with null. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AdditionWithNullTest()
        Dim a, b, sum As Amount

        ' Test both not null:
        a = New Amount(100.0, LengthUnits.Meter)
        b = New Amount(25.0, LengthUnits.Meter)
        sum = a + b
        Assert.AreEqual(New Amount(125.0, LengthUnits.Meter), sum)

        ' Test right not null:
        a = New Amount(100.0, LengthUnits.Meter)
        b = Nothing
        sum = a + b
        ' not consistent with .NET treatment of Nullable: 
        ' Assert.AreEqual(new Amount(100.0, LengthUnits.Meter), sum);
        Assert.IsNull(sum)

        ' Test left not null:
        a = Nothing
        b = New Amount(25.0, LengthUnits.Meter)
        sum = a + b
        ' not consistent with .NET treatment of Nullable: 
        ' Assert.AreEqual(new Amount(25.0, LengthUnits.Meter), sum);
        Assert.IsNull(sum)

        ' Test both null:
        a = Nothing
        b = Nothing
        sum = a + b
        Assert.AreEqual(Nothing, sum)
    End Sub

    ''' <summary> (Unit Test Method) tests subtract with null. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub SubtractWithNullTest()
        Dim a, b, subs As Amount

        ' Test both not null:
        a = New Amount(100.0, LengthUnits.Meter)
        b = New Amount(25.0, LengthUnits.Meter)
        subs = a - b
        Assert.AreEqual(New Amount(75.0, LengthUnits.Meter), subs)

        ' Test right not null:
        a = New Amount(100.0, LengthUnits.Meter)
        b = Nothing
        subs = a - b
        ' not consistent with .NET treatment of Nullable: 
        ' Assert.AreEqual(new Amount(100.0, LengthUnits.Meter), subs);
        Assert.IsNull(subs)

        ' Test left not null:
        a = Nothing
        b = New Amount(25.0, LengthUnits.Meter)
        subs = a - b
        ' not consistent with .NET treatment of Nullable: 
        ' Assert.AreEqual(new Amount(-25.0, LengthUnits.Meter), subs);
        Assert.IsNull(subs)

        ' Test both null:
        a = Nothing
        b = Nothing
        subs = a - b
        Assert.AreEqual(Nothing, subs)
    End Sub

    ''' <summary> (Unit Test Method) tests rounded comparison. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub RoundedComparisonTest()
        Dim a As New Amount(0.045, LengthUnits.Meter)
        Dim b As New Amount(0.0450000000001, LengthUnits.Meter)
        Dim c As New Amount(0.0450000000002, LengthUnits.Meter)
        Dim d As New Amount(0.046, LengthUnits.Meter)
        Assert.IsFalse(a.Value = b.Value)
        Assert.IsFalse(b.Value = c.Value)
        Assert.IsFalse(a.Value = c.Value)
        Assert.IsTrue(a = b)
        Assert.IsTrue(b = c)
        Assert.IsTrue(a = c)
        Assert.IsFalse(c = d)
        Assert.IsTrue(a.Equals(b))
        Assert.IsTrue(b.Equals(c))
        Assert.IsTrue(a.Equals(c))
        Assert.IsFalse(c.Equals(d))
    End Sub

    ''' <summary> (Unit Test Method) tests comparison 01. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod>
    Public Sub Comparison01Test()
        Dim value As Double = -0.00002
        Dim a As New Amount(value, EnergyUnits.Horsepower)
        Dim b As New Amount(value, EnergyUnits.Horsepower)

        Dim ar As Amount = a.ConvertedTo(EnergyUnits.Watt)
        Dim br As Amount = b.ConvertedTo(EnergyUnits.Watt)

        Assert.IsTrue(a = b, $"horsepower: {a} equals {b}")
        Assert.IsFalse(a > b, $"horsepower: {a} is not greater {b}")
        Assert.IsFalse(a < b, $"horsepower: {a} is not smaller than {b}")
        Assert.IsTrue(ar = br, $"watt: {ar} equals {br}")
        Assert.IsFalse(ar > br, $"watt: a {ar} is not larger than b {br}")
        Assert.IsFalse(ar < br, $"watt: a {ar} is not smaller than b {br}")
    End Sub

    ''' <summary> (Unit Test Method) tests comparison 02. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod>
    Public Sub Comparison02Test()
        Dim a As New Amount(120.0, SpeedUnits.KilometerPerHour)
        Dim b As New Amount(33.333333333, SpeedUnits.MeterPerSecond)

        Assert.IsTrue(a = b)
        Assert.IsFalse(a < b)
        Assert.IsFalse(a > b)
        Assert.IsTrue(a <= b)
        Assert.IsTrue(a >= b)
        Assert.IsFalse(a <> b)
    End Sub

    ''' <summary> (Unit Test Method) tests division by zero. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod>
    Public Sub DivisionByZeroTest()
        Dim d1 As New Amount(32.0, LengthUnits.Kilometer)
        Dim d2 As New Amount(0.0, LengthUnits.Kilometer)
        Dim t As New Amount(0.0, TimeUnits.Hour)

        Dim s As Amount

        s = d1 / t

        Assert.IsTrue(Double.IsInfinity(s.Value))
        Assert.IsTrue(Double.IsPositiveInfinity(s.Value))
        Assert.AreEqual(s.Unit, (d1.Unit / t.Unit))

        s = d2 / t

        Assert.IsTrue(Double.IsNaN(s.Value))
        Assert.AreEqual(s.Unit, (d2.Unit / t.Unit))
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests amount net data contract serializer serialization.
    ''' </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountNetDataContractSerializerSerializationTest()
        Dim a As New Amount(3500.12, EnergyUnits.KilowattHour * (365.0 * TimeUnits.Day) / VolumeUnits.CubicMeter)

        ' Serialize instance:
        Using stream As New MemoryStream()
            Dim serializer As New NetDataContractSerializer()
            serializer.Serialize(stream, a)

            ' Deserialize instance:
            stream.Position = 0
            Dim b As Amount = CType(serializer.Deserialize(stream), Amount)

            ' Show serialization (this closes the stream):
            stream.Position = 0
            Console.WriteLine(stream.ToXmlString())
            Console.WriteLine()

            ' Compare:
            Console.WriteLine(a)
            Console.WriteLine(b)
            Assert.AreEqual(a, b)
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests amount array net data contract serializer serialization.
    ''' </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountArrayNetDataContractSerializerSerializationTest()
        Dim aa(5) As Amount
        aa(0) = New Amount(32.5, LengthUnits.NauticalMile)
        aa(1) = New Amount(3500.12, EnergyUnits.KilowattHour * (365.0 * TimeUnits.Day) / VolumeUnits.CubicMeter)
        aa(2) = 3 * aa(0)
        aa(3) = 3 * aa(1)
        aa(4) = aa(1) / aa(3)
        aa(5) = New Amount(42.3, LengthUnits.Meter / TimeUnits.Second.Power(2))

        ' Serialize instance:
        Using stream As New MemoryStream()
            Dim serializer As New NetDataContractSerializer()
            serializer.WriteObject(stream, aa)

            ' Deserialize instance:
            stream.Position = 0
            Dim ba() As Amount = CType(serializer.ReadObject(stream), Amount())

            ' Show serialization (this closes the stream):
            stream.Position = 0
            Console.WriteLine(stream.ToXmlString())
            Console.WriteLine()

            ' Compare:
            Assert.AreEqual(aa.Length, ba.Length)
            For i As Integer = 0 To aa.Length - 1
                Console.WriteLine(aa(i))
                Console.WriteLine(ba(i))
                Assert.AreEqual(aa(i), ba(i))
            Next i
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests amount data contract serializer serialization.
    ''' </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountDataContractSerializerSerializationTest()
        Dim a As New Amount(3500.12, EnergyUnits.KilowattHour * (365.0 * TimeUnits.Day) / VolumeUnits.CubicMeter)

        ' Serialize instance:
        Using stream As New MemoryStream()
            Dim serializer As New DataContractSerializer(GetType(Amount))
            serializer.WriteObject(stream, a)

            ' Deserialize instance:
            stream.Position = 0
            Dim b As Amount = CType(serializer.ReadObject(stream), Amount)

            ' Show serialization (this closes the stream):
            stream.Position = 0
            Console.WriteLine(stream.ToXmlString())
            Console.WriteLine()


            ' Compare:
            Console.WriteLine(a)
            Console.WriteLine(b)
            Assert.AreEqual(a, b)
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests amount array data contract serializer serialization.
    ''' </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountArrayDataContractSerializerSerializationTest()
        Dim aa(5) As Amount
        aa(0) = New Amount(32.5, LengthUnits.NauticalMile)
        aa(1) = New Amount(3500.12, EnergyUnits.KilowattHour * (365.0 * TimeUnits.Day) / VolumeUnits.CubicMeter)
        aa(2) = 3 * aa(0)
        aa(3) = 3 * aa(1)
        aa(4) = aa(1) / aa(3)
        aa(5) = New Amount(42.3, LengthUnits.Meter / TimeUnits.Second.Power(2))

        ' Serialize instance:
        Using stream As New MemoryStream()

            Dim serializer As New DataContractSerializer(GetType(Amount()))
            serializer.WriteObject(stream, aa)

            ' Deserialize instance:
            stream.Position = 0
            Dim ba() As Amount = CType(serializer.ReadObject(stream), Amount())

            ' Show serialization (this closes the stream):
            stream.Position = 0
            Console.WriteLine(stream.ToXmlString())
            Console.WriteLine()

            ' Compare:
            Assert.AreEqual(aa.Length, ba.Length)
            For i As Integer = 0 To aa.Length - 1
                Console.WriteLine(aa(i))
                Console.WriteLine(ba(i))
                Assert.AreEqual(aa(i), ba(i))
            Next i

        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests amount binary formatter serialization. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountBinaryFormatterSerializationTest()
        Dim a As New Amount(3500.12, EnergyUnits.KilowattHour * (365.0 * TimeUnits.Day) / VolumeUnits.CubicMeter)

        ' Serialize instance:
        Using stream As New MemoryStream()
            Dim formatter As New BinaryFormatter()
            formatter.Serialize(stream, a)

            ' Deserialize instance:
            stream.Position = 0
            Dim b As Amount = CType(formatter.Deserialize(stream), Amount)

            ' Compare:
            Console.WriteLine(a)
            Console.WriteLine(b)
            Assert.AreEqual(a, b)
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests amount SOAP formatter serialization. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountSoapFormatterSerializationTest()
        Dim a As New Amount(3500.12, EnergyUnits.KilowattHour * (365.0 * TimeUnits.Day) / VolumeUnits.CubicMeter)

        ' Serialize instance:
        Using stream As New MemoryStream()
            Dim formatter As New SoapFormatter()
            formatter.Serialize(stream, a)

            ' Deserialize instance:
            stream.Position = 0
            Dim b As Amount = CType(formatter.Deserialize(stream), Amount)

            ' Show serialization (this closes the stream):
            stream.Position = 0
            Console.WriteLine(stream.ToXmlString())
            Console.WriteLine()

            ' Compare:
            Console.WriteLine(a)
            Console.WriteLine(b)
            Assert.AreEqual(a, b)
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests amount compatibility. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountCompatibilityTest()
        Dim a As New Amount(300, LengthUnits.Mile / TimeUnits.Hour.Power(2))
        Assert.IsTrue(a.Unit.IsCompatibleTo(LengthUnits.Meter / TimeUnits.Second.Power(2)))
        Assert.IsTrue(a.Unit.IsCompatibleTo(LengthUnits.Meter * TimeUnits.Second.Power(-2)))
        Assert.IsFalse(a.Unit.IsCompatibleTo(LengthUnits.Meter / TimeUnits.Second.Power(1)))
        Assert.IsFalse(a.Unit.IsCompatibleTo(MassUnits.Gram))
        Console.WriteLine(a.Unit.UnitType.ToString())
    End Sub

    ''' <summary> (Unit Test Method) tests amount split. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountSplitTest()
        ' One fifth of a week:
        Dim a As New Amount(1.0 / 5.0, TimeUnits.Day * 7.0)

        Dim result() As Amount = a.Split(New Unit() {TimeUnits.Day, TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second}, 3)

        For Each item As Amount In result
            Console.WriteLine(item)
        Next item

        Assert.AreEqual(4, result.Length)
        CollectionAssert.AreEqual(New Double() {1.0, 9.0, 36.0, 0.0}.ToList(), result.Select(Function(x) x.Value).ToList())
    End Sub

    ''' <summary> (Unit Test Method) tests amount split 2. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub AmountSplit2Test()
        ' One fifth of a week:
        Dim a As New Amount(7.0 / 5.0, TimeUnits.Day)

        Dim result() As Amount = a.Split(New Unit() {TimeUnits.Day, TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second}, 3)

        For Each item As Amount In result
            Console.WriteLine(item)
        Next item

        ' In this case, the split results in 1 day, 9 hours, 35 minutes and 60 SECONDS!
        ' This is due to rounding; it results in ..., 35 minutes and 59.99999 seconds,
        ' which once rounded, end up to be 60 seconds...

        Assert.AreEqual(4, result.Length)
        CollectionAssert.AreEqual(New Double() {1.0, 9.0, 35.0, 60.0}.ToList(), result.Select(Function(x) x.Value).ToList())
    End Sub

    ''' <summary> (Unit Test Method) tests amount split incompatible. </summary>
    ''' <remarks> David, 2020-03-07. </remarks>
    <TestMethod()>
    Public Sub AmountSplitIncompatibleTest()
        ' One fifth of a week:
        Dim a As New Amount(7.0 / 5.0, TimeUnits.Day)
        Dim amounts() As Amount = a.Split(New Unit() {TimeUnits.Day, TimeUnits.Hour, TimeUnits.Minute, TimeUnits.Second}, 3)
        Dim d As Amount = Amount.Combine(amounts, TimeUnits.Day)
        Assert.IsTrue(a.Equals(d), $"Combined amounts {d} should equals {a}")
    End Sub

End Class

Friend Module StreamExtensions

    ''' <summary> Converts a stream to an XML string. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    ''' <param name="stream"> The stream. </param>
    ''' <returns> Stream as a String. </returns>
    <System.Runtime.CompilerServices.Extension>
    Public Function ToXmlString(ByVal stream As Stream) As String
        Dim settings As New XmlWriterSettings() With {.OmitXmlDeclaration = True, .Indent = True}
        Using reader As New StreamReader(stream)
            Dim nt As New NameTable()
            Dim doc As New XmlDocument()
            doc.LoadXml(reader.ReadToEnd())
            Dim sb As New StringBuilder()
            Using xwri As XmlWriter = XmlWriter.Create(sb, settings)
                doc.WriteTo(xwri)
            End Using
            Return sb.ToString()
        End Using
    End Function
End Module
