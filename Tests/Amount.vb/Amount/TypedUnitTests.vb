''' <summary> This is a test class for typed units. </summary>
''' <remarks> David, 2020-10-05. </remarks>
<TestClass()>
Public Class TypedUnitTests

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

    #End Region

    ''' <summary> (Unit Test Method) tests unit equality. </summary>
    ''' <remarks> David, 2020-10-05. </remarks>
    <TestMethod()>
    Public Sub UnitEqualityTest()
        Dim leftAmpereUnit As Arebis.TypedUnits.Unit = Arebis.StandardUnits.ElectricUnits.Ampere
        Dim righAmpereUnit As Arebis.TypedUnits.Unit = Arebis.StandardUnits.ElectricUnits.Ampere
        Assert.IsTrue(leftAmpereUnit.Equals(righAmpereUnit), "Units are equal")
        Assert.IsTrue(leftAmpereUnit = righAmpereUnit, "Units are ==")
        Dim leftVoltUnit As Arebis.TypedUnits.Unit = Arebis.StandardUnits.ElectricUnits.Volt
        Assert.IsFalse(leftAmpereUnit.Equals(leftVoltUnit), "Units are not equal")
        Assert.IsTrue(leftAmpereUnit <> leftVoltUnit, "Units are <>")
    End Sub

End Class

