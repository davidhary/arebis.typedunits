using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Typed Units Scenario Tests")]
[assembly: AssemblyDescription( "Typed Units Scenario Tests" )]
[assembly: AssemblyProduct("Arebis.TypedUnits.Senario.Tests")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
