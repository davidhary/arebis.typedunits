using System.Reflection;
using System.Resources;

[assembly: AssemblyCompany( "Arebis" )]
[assembly: AssemblyCopyright( "(c) 2013 Rudi Breedenraedt." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
[assembly: NeutralResourcesLanguage( "en-US", UltimateResourceFallbackLocation.MainAssembly )]
[assembly: AssemblyCulture( "" )]


